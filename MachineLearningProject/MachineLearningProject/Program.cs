﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text.RegularExpressions;

namespace MachineLearningProject
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> sportWordsList = new List<string>();
            List<string> fairyTaleWordsList = new List<string>();
            List<List<int>> vectorList = new List<List<int>>();

            Dictionary<string, string> words = new Dictionary<string, string>();

            string[] sportFiles = Directory.GetFiles(@"C:\Users\Rasmus Rasmussen\Documents\GitLab\visualstudio\machinelearningproject\MachineLearningProject\MachineLearningProject\sports\");
            string[] fairyTaleFiles = Directory.GetFiles(@"C:\Users\Rasmus Rasmussen\Documents\GitLab\visualstudio\machinelearningproject\MachineLearningProject\MachineLearningProject\fairytales\");

            foreach (var file in fairyTaleFiles)
            {
                fairyTaleWordsList = File.ReadAllText(file).Split(' ').ToList();
            }

            for (int i = 0; i < fairyTaleWordsList.Count; i++)
            {
                fairyTaleWordsList[i] = Regex.Replace(fairyTaleWordsList[i], @"[^ÆØÅæøåa-zA-Z]+", "").ToLower();
                Console.WriteLine(fairyTaleWordsList[i]);
            }

            Console.WriteLine();

            foreach (var file in sportFiles)
            {
                sportWordsList = File.ReadAllText(file).Split(' ').ToList();
            }

            for (int i = 0; i < sportWordsList.Count; i++)
            {
                sportWordsList[i] = Regex.Replace(sportWordsList[i], @"[^ÆØÅæøåa-zA-Z]+", "").ToLower();
                Console.WriteLine(sportWordsList[i]);
            }

            foreach (var item in sportWordsList)
            {
                try
                {
                    words.Add(item, "sport");
                } catch(Exception e)
                {

                }
            }

            foreach (var item in fairyTaleWordsList)
            {
                try
                {
                    words.Add(item, "fairytale");
                }
                catch (Exception e)
                {

                }
            }

            //Create vectors for all sport texts.
            foreach (var file in sportFiles)
            {
                string text = Regex.Replace(File.ReadAllText(file).ToLower(), @"^[ÆØÅæøåA-Za-z ]", "");
                List<int> vector = new List<int>();

                foreach(var word in words)
                {
                    if(text.Contains(word.Key))
                    {
                        vector.Add(1);
                    } else
                    {
                        vector.Add(0);
                    }
                }
                vectorList.Add(vector);
            }

            //create vector for all fairy tale texts.
            foreach (var file in fairyTaleFiles)
            {
                string text = Regex.Replace(File.ReadAllText(file).ToLower(), @"[^ÆØÅæøåa-zA-Z ]+", "");
                List<int> vector = new List<int>();

                foreach (var word in words)
                {
                    if (text.Contains(word.Key))
                    {
                        vector.Add(1);
                    }
                    else
                    {
                        vector.Add(0);
                    }
                }
                vectorList.Add(vector);
            }

            string unknownText = Regex.Replace(File.ReadAllText(@"C:\Users\Rasmus Rasmussen\Documents\GitLab\visualstudio\machinelearningproject\MachineLearningProject\MachineLearningProject\unknowntext\unknowntext.txt").ToLower(), @"[^ÆØÅæøåa-zA-Z ]+", "");
            List<int> unknownTextVector = new List<int>();

            foreach (var word in words)
            {
                if (unknownText.Contains(word.Key))
                {
                    unknownTextVector.Add(1);
                }
                else
                {
                    unknownTextVector.Add(0);
                }
            }
        }
    }
}
