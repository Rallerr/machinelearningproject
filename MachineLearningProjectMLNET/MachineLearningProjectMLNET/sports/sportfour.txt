I stedet for en god startposition må Kevin Magnussen skæve til statistikken, når han finde motivation før Formel 1-løbet i morgen i Rusland.

Kevin Magnussen havde svært ved at få tilstrækkelig med vejgreb i kvalifikationen forud for Ruslands grandprix.

Årsagen var dækkene, der var svære at varme op, og kolde racerdæk giver altså en bil uden det ønskede vejgreb til sene nedbremsninger og høje svinghastigheder.

Derfor blev det blot til en 18.-plads i tidstagningen.

- Det var virkelig nogle dårlige omgange. Bilen er sindssyg god på tredje omgang på dækkene, men du har bare ikke tre omgange i en tidstagning. Der skal den være der på den første omgang, og hvis dækkene er vildt kolde der, så er det svært, sagde Kevin Magnussen til TV 3+.


Haas' teamchef, Günther Steiner, betragter da også kvalifikationen som en til glemmebogen.

- Det er et af de kapitler, hvor vores problemer fortsatte, siger holdchef Günther Steiner på Haas' hjemmeside.

- Lige nu kæmper vi videre og prøver at klemme så meget som muligt, ud af det vi har.

Kevin Magnussen kan dog glæde sig over, at Haas-raceren er god, når der er omgange nok til at få varme i dækkene, hvilket lover godt for selve racerløbet i morgen.

Samtidig kan den danske Formel 1-kører finde håb i statistikken, der viser, at Magnussen har fået point i fire ud af fem løb, han har kørt på banen i Rusland.

- Nu er jeg lige hoppet ud af bilen og er skuffet over det her resultat, men jeg vil prøve igen i morgen, sagde Magnussen til TV 3+.


Ruslands grandprix bliver kørt i morgen kl. 13.10 og kan følges i en liveblog her på siden.